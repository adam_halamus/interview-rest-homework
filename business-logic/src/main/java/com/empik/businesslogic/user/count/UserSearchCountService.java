package com.empik.businesslogic.user.count;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class UserSearchCountService {

    private UserSearchCountRepository userSearchCountRepository;

    public UserSearchCount addUserSearch(String login) {
        UserSearchCount userSearchCount = userSearchCountRepository.findByLogin(login)
                                                                   .orElse(new UserSearchCount(login));
        userSearchCount.incrementRequestCount();
        userSearchCountRepository.save(userSearchCount);

        return userSearchCount;
    }
}
