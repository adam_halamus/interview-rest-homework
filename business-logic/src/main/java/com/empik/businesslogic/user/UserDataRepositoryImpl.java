package com.empik.businesslogic.user;

import com.empik.user.User;
import com.empik.user.UserClient;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
@AllArgsConstructor
class UserDataRepositoryImpl implements UserDataRepository {

    private final UserClient userClient;

    @Override
    public Optional<UserDTO> getUserDTO(String login) {
        User user = userClient.getUser(login);

        if (user == null) {
            return Optional.empty();
        }

        return Optional.of(
                new UserDTO(
                        user.getId(),
                        user.getLogin(),
                        user.getName(),
                        user.getType(),
                        user.getAvatarUrl(),
                        user.getPublicRepos(),
                        user.getFollowers(),
                        user.getCreatedAt()
                )
                          );
    }
}
