package com.empik.businesslogic.user;

public class MissingLoginException extends RuntimeException{

    public MissingLoginException() {
        super("Missing parameter: Must provide 'login' parameter.");
    }
}
