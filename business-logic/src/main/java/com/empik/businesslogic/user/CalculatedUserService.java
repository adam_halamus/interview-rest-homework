package com.empik.businesslogic.user;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class CalculatedUserService {

    private UserDataRepository userDataRepository;

    public CalculatedUserDTO getUser(String login) {
        if (login == null) {
            throw new MissingLoginException();
        }

        UserDTO userData = userDataRepository.getUserDTO(login)
                                             .orElseThrow(() -> new NoUserFoundException(login));

        return new CalculatedUserDTO(
                userData.getId(),
                userData.getLogin(),
                userData.getName(),
                userData.getType(),
                userData.getAvatarUrl(),
                userData.getPublicRepos(),
                userData.getFollowers(),
                userData.getCreatedAt()
        );
    }
}
