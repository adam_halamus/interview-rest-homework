package com.empik.businesslogic.user;

import lombok.Data;
import lombok.Value;

import java.time.LocalDateTime;

@Value
public class CalculatedUserDTO {

    private Long id;

    private String login;

    private String name;

    private String type;

    private String avatarUrl;

    private Double calculations;

    private LocalDateTime createdAt;

    public CalculatedUserDTO(Long id, String login, String name, String type, String avatarUrl, Integer publicRepos, Integer followers, LocalDateTime createdAt) {
        this.id = id;
        this.login = login;
        this.name = name;
        this.type = type;
        this.avatarUrl = avatarUrl;
        this.calculations = UserCalculator.getCalculations(publicRepos, followers);
        this.createdAt = createdAt;
    }

    private class UserCalculator {

        static Double getCalculations(Integer publicRepos, Integer followers) {
            double result = (float) 6 / followers * (2 + publicRepos);
            return result;
        }
    }
}
