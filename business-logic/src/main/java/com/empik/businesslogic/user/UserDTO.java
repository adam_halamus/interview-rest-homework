package com.empik.businesslogic.user;

import lombok.Value;

import java.time.LocalDateTime;

@Value
class UserDTO {

    private final Long id;

    private final String login;

    private final String name;

    private final String type;

    private final String avatarUrl;

    private final Integer publicRepos;

    private final Integer followers;

    private final LocalDateTime createdAt;
}