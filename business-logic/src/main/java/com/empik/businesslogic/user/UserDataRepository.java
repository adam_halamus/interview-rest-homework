package com.empik.businesslogic.user;

import java.util.*;

interface UserDataRepository {

    Optional<UserDTO> getUserDTO(String login);
}
