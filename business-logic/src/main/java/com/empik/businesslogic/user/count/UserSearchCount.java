package com.empik.businesslogic.user.count;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Entity
@Getter
@NoArgsConstructor
@AllArgsConstructor
class UserSearchCount {

    public UserSearchCount(String login) {
        this.login = login;
        this.requestCount = 0;
    }

    @Id
    @GeneratedValue
    private Long id;

    @Column(nullable = false, unique = true)
    private String login;

    @Column
    private Integer requestCount;

    void incrementRequestCount() {
        this.requestCount = this.requestCount + 1;
    }
}
