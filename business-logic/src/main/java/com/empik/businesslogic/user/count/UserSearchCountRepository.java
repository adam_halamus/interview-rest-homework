package com.empik.businesslogic.user.count;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.*;

@Repository
public interface UserSearchCountRepository extends JpaRepository<UserSearchCount, Long> {

    Optional<UserSearchCount> findByLogin(String login);
}
