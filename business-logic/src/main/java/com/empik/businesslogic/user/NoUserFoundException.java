package com.empik.businesslogic.user;

public class NoUserFoundException extends RuntimeException {

    public NoUserFoundException(String login) {
        super("User with login '" + login + "' was not found.");
    }
}
