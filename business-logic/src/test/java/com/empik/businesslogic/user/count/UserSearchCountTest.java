package com.empik.businesslogic.user.count;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class UserSearchCountTest {

    @Test
    void testIncrementRequestCount() {
        //given:
        UserSearchCount userSearchCount = new UserSearchCount("Foo");

        //when:
        userSearchCount.incrementRequestCount();
        userSearchCount.incrementRequestCount();

        //then:
        assertEquals(2, userSearchCount.getRequestCount());
    }

    @Test
    void testNewUserSearchCount() {
        //given:
        UserSearchCount userSearchCount = new UserSearchCount("Foo");

        //then:
        assertEquals(0, userSearchCount.getRequestCount());
    }
}