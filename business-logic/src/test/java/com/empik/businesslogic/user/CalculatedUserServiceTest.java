package com.empik.businesslogic.user;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDateTime;
import java.util.*;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class CalculatedUserServiceTest {

    @Mock
    private UserDataRepository userDataRepository;

    @InjectMocks
    private CalculatedUserService userService;

    @Test
    void testGetUser() {
        //given:
        String login = "foo";
        UserDTO userDTO = new UserDTO(1L, login, "name", "type", "url", 1, 6, LocalDateTime.MIN);

        //when:
        when(userDataRepository.getUserDTO(login)).thenReturn(Optional.of(userDTO));
        CalculatedUserDTO result = userService.getUser(login);

        //then:
        assertEquals(1L, result.getId());
        assertEquals(login, result.getLogin());
        assertEquals("name", result.getName());
        assertEquals("type", result.getType());
        assertEquals("url", result.getAvatarUrl());
        assertEquals(3L, result.getCalculations());
        assertEquals(LocalDateTime.MIN, result.getCreatedAt());
    }

    @Test
    void testNoUserFound() {
        //when:
        when(userDataRepository.getUserDTO(any())).thenReturn(Optional.empty());

        Assertions.assertThrows(NoUserFoundException.class, () -> {
            userService.getUser("Foo");
        });
    }

    @Test
    void testLoginProvided() {
        //when:
        Assertions.assertThrows(MissingLoginException.class, () -> {
            userService.getUser(null);
        });
    }
}