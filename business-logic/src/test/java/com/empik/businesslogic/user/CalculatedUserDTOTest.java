package com.empik.businesslogic.user;

import org.junit.jupiter.api.Test;

import java.time.LocalDateTime;

import static org.junit.jupiter.api.Assertions.assertEquals;

class CalculatedUserDTOTest {

    @Test
    void testCreateNew() {
        //when:
        CalculatedUserDTO calculatedUserDTO = new CalculatedUserDTO(
                1L,
                "Foo",
                "name",
                "type",
                "url",
                1,
                6,
                LocalDateTime.MIN
        );

        //then:
        assertEquals(1L, calculatedUserDTO.getId());
        assertEquals("Foo", calculatedUserDTO.getLogin());
        assertEquals("name", calculatedUserDTO.getName());
        assertEquals("type", calculatedUserDTO.getType());
        assertEquals("url", calculatedUserDTO.getAvatarUrl());
        assertEquals(3L, calculatedUserDTO.getCalculations());
        assertEquals(LocalDateTime.MIN, calculatedUserDTO.getCreatedAt());
    }
}