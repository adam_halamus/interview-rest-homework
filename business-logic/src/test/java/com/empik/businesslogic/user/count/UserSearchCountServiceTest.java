package com.empik.businesslogic.user.count;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.*;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class UserSearchCountServiceTest {

    @Mock
    private UserSearchCountRepository userSearchCountRepository;

    @InjectMocks
    private UserSearchCountService userSearchCountService;

    @Test
    void testNewUserSearch() {
        //given:
        String login = "foo";

        //when:
        when(userSearchCountRepository.findByLogin(login)).thenReturn(Optional.empty());
        UserSearchCount result = userSearchCountService.addUserSearch(login);

        //then:
        verify(userSearchCountRepository).save(result);
        assertEquals(1, result.getRequestCount());
        assertEquals(login, result.getLogin());
    }

    @Test
    void testExistingUserSearch() {
        //given:
        String login = "foo";
        UserSearchCount user = new UserSearchCount(1L, login, 1);

        //when:
        when(userSearchCountRepository.findByLogin(login)).thenReturn(Optional.of(user));
        UserSearchCount result = userSearchCountService.addUserSearch(login);

        //then:
        verify(userSearchCountRepository).save(user);
        assertEquals(2, result.getRequestCount());
        assertEquals(login, result.getLogin());
    }
}