package com.empik.businesslogic.user;

import com.empik.user.User;
import com.empik.user.UserClient;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDateTime;
import java.util.*;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class UserDataRepositoryImplTest {

    @Mock
    private UserClient userClient;

    @InjectMocks
    private UserDataRepositoryImpl userDataRepository;

    @Test
    void testGetEmptyUserDTO() {
        //given:
        String nonExistingLogin = "Foo";

        //when:
        when(userClient.getUser(nonExistingLogin)).thenReturn(null);
        Optional<UserDTO> result = userDataRepository.getUserDTO(nonExistingLogin);

        //then:
        assertTrue(result.isEmpty());
    }

    @Test
    void testGetUserDTO() {
        //given:
        String login = "Foo";
        User user = new User(1L, login, "name", "type", "url", 1, 6, LocalDateTime.MIN);

        //when:
        when(userClient.getUser(login)).thenReturn(user);
        Optional<UserDTO> result = userDataRepository.getUserDTO(login);

        //then:
        assertEquals(1L, result.get().getId());
        assertEquals(login, result.get().getLogin());
        assertEquals("name", result.get().getName());
        assertEquals("type", result.get().getType());
        assertEquals("url", result.get().getAvatarUrl());
        assertEquals(1, result.get().getPublicRepos());
        assertEquals(6, result.get().getFollowers());
        assertEquals(LocalDateTime.MIN, result.get().getCreatedAt());
    }
}