package com.empik.rest;

import com.empik.businesslogic.user.CalculatedUserDTO;
import lombok.Value;

import java.time.LocalDateTime;

@Value
public class UserResult {

    private Long id;

    private String login;

    private String name;

    private String type;

    private String avatarUrl;

    private Double calculations;

    private LocalDateTime createdAt;

    public UserResult(CalculatedUserDTO calculatedUserDTO) {
        this.id = calculatedUserDTO.getId();
        this.login = calculatedUserDTO.getLogin();
        this.name = calculatedUserDTO.getName();
        this.type = calculatedUserDTO.getType();
        this.avatarUrl = calculatedUserDTO.getAvatarUrl();
        this.calculations = calculatedUserDTO.getCalculations();
        this.createdAt = calculatedUserDTO.getCreatedAt();
    }
}
