package com.empik.rest;

public interface UserService {

    public UserResult getUser(String login);
}
