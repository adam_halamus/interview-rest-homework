package com.empik.rest;

import com.empik.businesslogic.user.count.UserSearchCountService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/users")
@AllArgsConstructor
public class UserController {

    private UserService userService;

    private UserSearchCountService userSearchCountService;

    @GetMapping(value = "/{login}")
    public UserResult getUser(@PathVariable String login) {
        userSearchCountService.addUserSearch(login);
        return userService.getUser(login);
    }
}
