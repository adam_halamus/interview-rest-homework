package com.empik.rest;

import com.empik.businesslogic.user.CalculatedUserDTO;
import com.empik.businesslogic.user.CalculatedUserService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class UserServiceImpl implements UserService {

    private CalculatedUserService calculatedUserService;

    @Override
    public UserResult getUser(String login) {
        CalculatedUserDTO user = calculatedUserService.getUser(login);

        return new UserResult(user);
    }
}
