package com.empik.user.com.empik.user.config;

import lombok.Getter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@Getter
@Configuration
@PropertySource("classpath:url.properties")
public class UserApiConnectionProperties {

    @Value("${user}")
    private String userGetUrl;
}
