package com.empik.user;

import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@NoArgsConstructor
@AllArgsConstructor
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
public class User {

    private Long id;

    private String login;

    private String name;

    private String type;

    private String avatarUrl;

    private Integer publicRepos;

    private Integer followers;

    private LocalDateTime createdAt;
}
