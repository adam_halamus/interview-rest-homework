package com.empik.user;

import com.empik.user.com.empik.user.config.UserApiConnectionProperties;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.reactive.function.client.WebClientResponseException;
import reactor.core.publisher.Mono;

@Component
public class UserClient {

    private UserApiConnectionProperties userApiConnectionProperties;

    public UserClient(UserApiConnectionProperties userApiConnectionProperties) {
        this.userApiConnectionProperties = userApiConnectionProperties;
    }

    public User getUser(String login) {
        WebClient webClient = WebClient.builder()
                                       .build();

        return webClient.get()
                        .uri(userApiConnectionProperties.getUserGetUrl(), login)
                        .retrieve()
                        .bodyToMono(User.class)
                        .onErrorResume(WebClientResponseException.class,
                                       ex -> ex.getRawStatusCode() == 404 ? Mono.empty() : Mono.error(ex))
                        .block();
    }
}
