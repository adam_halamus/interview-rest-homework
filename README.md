Project has a H2 in-memory database just to be easier to run.

Project build with:
Apache Maven 3.8.4
Java version: 17.0.3.1

To build execute `mvn clean install`
To run execute `mvn spring-boot:run`
